/**
 * BLOCK: Adchitects Slider
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const {
	__,
} = wp.i18n;
const {
	registerBlockType,
} = wp.blocks;
const {
	Button,
	IconButton,
	PanelBody,
	TextControl,
} = wp.components;
const {
	InspectorControls,
} = wp.editor;
const {
	Fragment,
} = wp.element;

/**
 * Register: Adchitects Slider Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'grf/gutenberg-adchitects-slider', {
	title: __( 'Adchitects Slider' ),
	icon: 'smiley',
	category: 'common',
	attributes: {
		items: {
			type: 'array',
			default: [],
		},
	},
	edit: ( props ) => {
		const handleAddItem = () => {
			const items = [ ...props.attributes.items ];
			items.push( {
				address: '',
			} );
			props.setAttributes( { items } );
		};

		const handleRemoveItem = ( index ) => {
			const items = [ ...props.attributes.items ];
			items.splice( index, 1 );
			props.setAttributes( { items } );
		};

		const handleItemChange = ( address, index ) => {
			const items = [ ...props.attributes.items ];
			items[ index ].address = address;
			props.setAttributes( { items } );
		};

		let itemFields,
			itemDisplay;

		if ( props.attributes.items.length ) {
			itemFields = props.attributes.items.map( ( item, index ) => {
				return <Fragment key={ index }>
					<TextControl
						className="grf__item-address"
						placeholder="350 Fifth Avenue New York NY"
						value={ props.attributes.items[ index ].address }
						onChange={ ( address ) => handleItemChange( address, index ) }
					/>
					<IconButton
						className="grf__remove-item-address"
						icon="no-alt"
						label="Delete item"
						onClick={ () => handleRemoveItem( index ) }
					/>
				</Fragment>;
			} );

			itemDisplay = props.attributes.items.map( ( item, index ) => {
				return <p key={ index }>{ item.address }</p>;
			} );
		}

		return [
			<InspectorControls key="1">
				<PanelBody title={ __( 'Items' ) }>
					{ itemFields }
					<Button
						isDefault
						onClick={ handleAddItem.bind( this ) }
					>
						{ __( 'Add Item' ) }
					</Button>
				</PanelBody>
			</InspectorControls>,
			<div key="2" className={ props.className }>
				<h2>Block</h2>
				<div className="media">
					{ itemDisplay }
				</div>
			</div>,
		];
	},
	save: ( props ) => {
		const itemFields = props.attributes.items.map( ( item, index ) => {
			return <p key={ index }>{ item.address }</p>;
		} );

		return (
			<div className={ props.className }>
				<h2>Block</h2>
				<div className="media">
					{ itemFields }
				</div>
			</div>
		);
	},
} );
